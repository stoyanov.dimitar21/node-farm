## Build the image 
docker build -t node-example .

## Run the container
docker run -d --rm --name node-cont -p 80:80 node-example