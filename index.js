const fs = require('fs');
const http = require('http');
const url = require('url');
const replaceTemplate = require('./modules/replaceTemplate');


const overviewTemp = fs.readFileSync(`${__dirname}/templates/overview.html`, 'utf-8');
const productTemp  = fs.readFileSync(`${__dirname}/templates/product.html`, 'utf-8');
const cardTemp     = fs.readFileSync(`${__dirname}/templates/template-card.html`, 'utf-8');

const data = fs.readFileSync(`${__dirname}/dev-data/data.json`, 'utf-8');
const dataObj = JSON.parse(data);

const server = http.createServer((req, res) => {
    const { pathname, query } = url.parse(req.url, true);

    // Oveview page
    if (pathname === '/'  || pathname === '/overview') {
        res.writeHead(200, { 'Content-type': 'text/html'});

        const cardsHtml = dataObj.map(el => replaceTemplate(cardTemp, el)).join('');
        const output = overviewTemp.replace('{%TEMPLATE_CARDS%}', cardsHtml);

        res.end(output);
    
    // Product page
    } else if (pathname === '/product') {
        res.writeHead(200, { 'Content-type': 'text/html'});
        const product = dataObj[query.id];
        const output = replaceTemplate(productTemp, product);

        res.end(output);
        
    // API
    } else if (pathname === '/api') {
        res.writeHead(200, { 'Content-type': 'application/json' });
        res.end(data);

    // Not found
    } else {
        res.writeHead(404, { 'Content-type': 'text/html', 'my-own-header': '404 Not Found' });
        res.end('<h1>Not found</h1>')
    }
});

server.listen(80, () => {
    console.log('Server start');
});
